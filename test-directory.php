#!/usr/bin/php
<?php

require_once("lib/ATCMLImporter.php");

// verify that we have a directory as argument[1]
if (count($argv) == 1) {
	echo "ERROR - missing argument 'directory' to test\n";
	echo "wich directory inside 'samples_atcml' would you test.\n";
	die();
}
$dir = $argv[1];

function error_handling($file, $message, $exception) {
	echo "---------------ERROR-----------------\n";
	echo $file."\n";
	echo $message."\n";
	echo $exception."\n";
	echo "---------------\n";
}

function success_handling($content, $file) {
	echo "---------------SUCCESS-----------------\n";
	echo $file ."\n";
	echo $content['title'] ."\n";
	echo "---------------------------------------\n";
}

$importer = new ATCMLImporter();
$importer->addErrorCallback('error_handling');
$importer->addSuccessCallback('success_handling');

echo "copy 'samples_atcml/" . $dir . "' directory to 'atcfm_work/" . $dir . "'\n";
rcopy("samples_atcml/" . $dir, "atcml_work/" . $dir);

echo "parse contents ... \n";

$contents = $importer->parseDirectory('atcml_work/' . $dir);

echo count($contents) . " found and loaded \n";
foreach($contents as $content) {
/*	echo $content['publicationDate']->format('Y-m-d H:i') . ": " . $content['title'] . "\n";
	echo "item -----------------------------------\n";
	echo $importer->loadText($content);
	echo "attachments -----------------------------------\n";

	echo "\n###################################\n";
*/
	$importer->cleanAllFiles($content, true);
}

?>
